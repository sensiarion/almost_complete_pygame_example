from typing import List, TypeVar, Tuple, Type, Optional

import pygame

from pygame_utils.core import RenderableObject, Game

Generic = TypeVar('Generic')


class BoardElement(RenderableObject):
    """
    Класс для отображения ячейки доски в игре
    """

    border_color = 255, 255, 255
    border_width = 1

    def __init__(self, left_top: Tuple[int, int], array_pos: Tuple[int, int], board: 'Board', size: int = 30):
        """
        :param left_top: координаты верхнего левого угла ячейки
        :param array_pos: позиция элемента в массиве доски
        :param board: экземпляр доски на котором создаётся ячейка
        :param size: размер стороны яечки
        """
        self.size = size
        self.array_pos = array_pos
        self.left_top = left_top
        self.board = board

        self.rect = pygame.Rect(self.left_top[0] + 1, self.left_top[1] + 1, self.size - 2, self.size - 2)

        self.clicked = False

    def on_click(self):
        """
        Обработчик для события нажатия на ячейку.
        Можно переопределить
        :return:
        """
        self.clicked = not self.clicked

    def setup(self, game: 'Game'):
        """
        Выставление начальных параметров для объекта
        :param game: экземпляр игры
        """
        pass

    def render(self, screen: pygame.Surface, time_delta: int):
        """
        Стандартный метод для отрисовки элемента.

        Данный метод можно переопределить в дочерних классах для установки своего метода отрисовки.
        Также, можно вызывать данную функцию через функцию **super()**в переопределённой функции
         для совмещения стандартной логики отрисовки и специфичной для вашей игры

        :param screen: полотно, на котором происходит отрисовка
        :param time_delta: временной сдвиг с момента отрисовки предыдущего кадра в миллисекундах
        """
        pygame.draw.rect(screen, self.border_color, self.rect, self.border_width)


class Board(RenderableObject):
    """
    Класс для работы с доской
    """
    color = (255, 255, 255)  # цвет дл рамок доски
    active = (255, 0, 0)
    width = 1  # толщина рамки

    cell_size: int
    top: int
    left: int

    def __init__(self, width: int = 5, height: int = 6, left: int = 10,
                 top: int = 10, cell_size: int = 80, board_element_class: Type[Generic] = BoardElement):
        """
        :param width: кол-во элементов в строке
        :param height: кол-во строк в доске
        :param left: координата верхнего левого угла доски по горизонтали
        :param top: координата верхнего левого угла доски по вертикали
        :param cell_size: размер одной ячейки доски
        :param board_element_class: класс, использующийся для создания ячеек на доске
        """
        self.board_element_class = board_element_class
        self.height = height
        self.width = width

        self.set_view(left, top, cell_size)

        self.board: List[List[Generic]] = self.create_board()

    def create_board(self) -> List[List[Generic]]:
        """
        Создание элементов доски и заполнение списка для их хранения
        :return: созданный двумерный список с ячейками доски
        """
        board = []
        for i in range(self.width):
            row = []
            for j in range(self.height):
                left = self.left + i * self.cell_size
                top = self.top + j * self.cell_size
                row.append(self.board_element_class((left, top), (i, j), self, self.cell_size + 2))
            board.append(row)

        return board

    def set_view(self, left: int, top: int, cell_size: int):
        """
        Изменяет визуальные характеристики доски

        :param left:  координата верхнего левого угла доски по горизонтали
        :param top: координата верхнего левого угла доски по вертикали
        :param cell_size: размер ячейки
        """
        self.cell_size = cell_size
        self.top = top
        self.left = left

    def get_board_bounds(self) -> pygame.Rect:
        """
        Получает границы доски для отрисовки
        :return: объект прямоугольника по габаритам доски
        """
        rect = pygame.Rect(self.left, self.top, self.cell_size * self.width, self.cell_size * self.height)
        return rect

    def get_cell(self, mouse_pos: Tuple[int, int]) -> Optional[Tuple[int, int]]:
        """
        Получает определённую ячейку, если указанные координаты находятся на этой ячейке
        :param mouse_pos: координаты для сопоставления с ячейкой
        :return: позиция ячейки во внутреннем списке ячеек
        """
        board_bounds = self.get_board_bounds()
        if board_bounds.collidepoint(*mouse_pos):
            x = (mouse_pos[0] - self.left) // self.cell_size
            y = (mouse_pos[1] - self.top) // self.cell_size

            return x, y

    def on_click(self, cell_coords: Tuple[int, int]):
        """
        Обработчик для нажатия мышки на доску.

        Передаёт событие ячейки, на которой произошло нажатие
        :param cell_coords: координаты ячейки, для которой необходимо выоплнить обработку нажатия
        """
        i, j = cell_coords
        return self.board[i][j].on_click()

    def get_click(self, mouse_pos):
        """
        Выполняет действие доски по нажтию или навеледению мышки
        :param mouse_pos:
        :return:
        """
        cell = self.get_cell(mouse_pos)
        if cell:
            self.on_click(cell)

    def setup(self, game: 'Game'):
        """
        Инициализация доски
        :param game: экземпляр игры, в которой создаётся доска
        """
        game.add_handler(pygame.MOUSEBUTTONDOWN, lambda event: self.get_click(event.pos))

    def render(self, screen: pygame.Surface, time_delta: int):
        """
        Метод по отрисовке доски.

        :param screen: полотно, на котором необходимо отрисовать доску
        :param time_delta: временной сдвиг от последнего кадра в миллисекундах
        """
        for i in range(self.width):
            for j in range(self.height):
                value = self.board[i][j]
                value.render(screen, time_delta)
