import os
from abc import abstractmethod, ABC
from collections import defaultdict
from typing import Callable, List, Dict, Tuple, Optional

import pygame


def load_image(path: str, directory: str = 'assets',
               size: Optional[Tuple[int, int]] = None) -> pygame.Surface:
    """
    Загружает изображение
    :param path:
    :param directory:
    :param size:
    :return:
    """
    full_path = os.path.join(directory, path)
    if not os.path.isfile(full_path):
        raise FileNotFoundError(f"Unable to find path to image: {full_path}")

    image = pygame.image.load(full_path)
    if size is not None:
        image = pygame.transform.scale(image, size)

    return image


class RenderableObject(ABC):
    """
    Абстракный класс для описание минимального элемента, который может быть отображён на экране
    """

    @abstractmethod
    def render(self, screen: pygame.Surface, time_delta: int):
        """
        Выполняется каждрый раз при отрисовке кадра.

        Его необходимо переопределить
        :param screen: полотно для отрисовки
        :param time_delta: время от последнего кадра в миллисекундах
        :return:
        """
        pass

    @abstractmethod
    def setup(self, game: 'Game'):
        """
        Вызывается при создании объекта
        :param game: полотно для отрисовки
        """
        pass

    @abstractmethod
    def update(self, game: 'Game'):
        """
        Итерация игры, обновляющая состояние объекта
        :param game:
        :return:
        """
        pass


class ImageObject(RenderableObject, ABC):
    """
    Класс для отрисовки изображения
    """

    def __init__(self, path: str, coords: Tuple[int, int], directory='assets', size: Tuple[int, int] = None):
        """
        :param path: путь до файла
        :param coords: координаты для отрисовки изображения
        :param directory: папка, в которой находятся изображения
        :param size: размер изображения
        """
        super().__init__()

        self.size = size
        self.directory = directory
        self.path = path

        self.image = load_image(path, directory, size)
        self.rect = pygame.Rect(coords[0], coords[1], *self.image.get_size())

    def render(self, screen: pygame.Surface, time_delta: int):
        screen.blit(self.image, self.rect)


class SpriteGroup(RenderableObject, pygame.sprite.Group):
    """
    Класс для отрисовки группы спрайтов. Сами по себе спрайты недоступны для отрисовки и
    подлежат группировке в набор объектов, который уже может быть отрисован
    """

    def setup(self, game: 'Game'):
        pass

    def update(self, game: 'Game'):
        pygame.sprite.Group.update(self, game)
        # for sprite in self.sprites():
        #     sprite.update(game)

    def render(self, screen: pygame.Surface, time_delta: int):
        self.draw(screen)


class SpriteObject(pygame.sprite.Sprite):
    """
    Класс для работы со спрайтом. Любой спрайт ассоциируются с некоторым изображением,
    поэтому для урпощения жизни были добавлены параметры для создания изображения вместе с спрайтом
    """

    def __init__(self, image_path: str, coords: Tuple[int, int], image_dir: str = 'assets',
                 size: Tuple[int, int] = None, *groups: pygame.sprite.AbstractGroup):
        super().__init__(*groups)
        self.image_dir = image_dir
        self.image_path = image_path

        self.image = load_image(image_path, image_dir, size)
        self.rect = pygame.Rect(coords[0], coords[1], *self.image.get_size())

    def update(self, game: 'Game') -> None:
        pass


class AnimatedSprite(pygame.sprite.Sprite):
    """
    Простенький класс для анимированных спрайтов
    """

    def __init__(self, image_paths: List[str], coords: Tuple[int, int], image_dir: str = 'assets',
                 size: Tuple[int, int] = None, *groups: pygame.sprite.AbstractGroup):
        super().__init__(*groups)
        self.image_dir = image_dir
        self.image_paths = sorted(image_paths)

        if len(image_paths) == 0:
            raise ValueError("At least 1 sprite should be specified for animated sprite")

        self.images: List[pygame.Surface] = []

        for sprite_path in self.image_paths:
            self.images.append(load_image(sprite_path, image_dir, size))

        self.__speed = 1.0
        self.__started = False
        self.__counter = 0
        self._index = 0

        self.image = self.images[self._index]
        self.rect = pygame.Rect(coords[0], coords[1], *self.images[0].get_size())

    def start(self, speed: float = 1):
        """
        Начинает воспроизводить анимацию
        """
        self.__started = True
        self.__speed = speed * 0.1

    def stop(self):
        """
        Останавливает воспроизведение анимации
        """
        self.__started = False

    def is_started(self):
        """
        Проверка на то, запущена ли анимация
        :return:
        """
        return self.__started

    def update(self, game: 'Game') -> None:
        """
        Простенький вариант для работы анимации
        """
        if self.__started:
            # увеличиваем счётчик на каждой итерации на некотое небольшое значение
            self.__counter += self.__counter + 1 * self.__speed
            if self.__counter >= 1:
                # если счётчик дошёл до отмечки, то выставляем следующее в списке изображение на отрисовку
                self._index = (self._index + 1) % len(self.images)
                self.__counter = 0

            self.image = self.images[self._index]


class Game(ABC):
    """
    Основной класс для игры
    """
    screen: pygame.Surface

    def __init__(self, width: int = 600, height: int = 400, name: str = 'Game', fps: int = 60):
        """
        Размеры окна игры
        :param width: ширина окна
        :param height: высота окна
        :param name: название для окна
        :param fps: кол-во кадров в секунду в окне
        """
        self.fps = fps
        self.name = name
        self.height = height
        self.width = width

        self.clock = pygame.time.Clock()

        self._running = False

        self.__objects: List[RenderableObject] = []
        self.__groups: List[SpriteGroup] = []

        annotation = Callable[[pygame.event.Event], None]
        self._handlers: Dict[int, List[annotation]] = defaultdict(list)
        self.setup()

    @abstractmethod
    def setup(self):
        """
        Метод, выполняемый при инициализации объекта игры.

        В нём можно создавать внутрениие объекты игры и инициализировать все необходимые
        для начала игры переменные
        """
        pass

    @abstractmethod
    def draw(self, time_delta: int):
        """
        Метод по отрисовке окна игры.

        Вызывается каждый раз перед отрисовкой объектов игры
        :param time_delta: время в миллисекндах с отрисовки предыдущего кадра
        """
        pass

    def update(self):
        """
        Обновляет состояние всех игорвых объектов
        """
        for obj in self.__objects:
            obj.update(self)

    def is_running(self) -> bool:
        """
        Проверка на то, запущенна ли игра
        """
        return self._running

    def _draw(self, time_delta: int):
        for object in self.__objects:
            object.render(self.screen, time_delta)

    def add_object(self, obj: RenderableObject):
        """
        Добавляет объект для отрисовки на экран.

        Должен быть экземпляром класса или наследника класса RenderableObject
        :param obj: созданный объект для отрисовки
        """
        obj.setup(self)
        self.__objects.append(obj)
        if isinstance(obj, SpriteGroup):
            self.__groups.append(obj)

    def get_objects(self) -> List[RenderableObject]:
        return self.__objects

    def get_sprite_groups(self) -> List[SpriteGroup]:
        return self.__groups

    def clear(self) -> Tuple[List[RenderableObject], List[SpriteGroup]]:
        objects = self.__objects
        groups = self.__groups
        self.__objects = []
        self.__groups = []

        return objects, groups

    def set(self, objects: List[RenderableObject], groups: List[SpriteGroup]):
        self.__objects = objects
        self.__groups = groups

    def add_handler(self, event_type: int, handler: Callable):
        """
        Добавляет функцию обработчик для действия.

        Обработчик будет запущен при наступлении указанного события
        :param event_type: константна события, по которой можно опознать событие (например pygame.MOUSEBUTTONDOWN)
        :param handler: функция обработчик, которой будет передан аргумент с событием
        :return:
        """

        self._handlers[event_type].append(handler)

    def run(self):
        """
        Метод для запуска игры
        """
        pygame.init()
        pygame.display.set_caption(self.name)
        self.screen = pygame.display.set_mode((self.width, self.height))
        self._running = True
        while self._running:
            self.screen.fill((0, 0, 0))

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self._running = False
                for handler in self._handlers.get(event.type, []):
                    handler(event)

            time_delta = self.clock.tick(self.fps)

            self.update()

            self.draw(time_delta)
            self._draw(time_delta)



            pygame.display.flip()
        pygame.quit()


class Text(RenderableObject):
    """
    Обёртка вокруг текста для более удобной отрисовке на экране
    """

    def __init__(self, text, pos: Tuple[int, int], font_size=20, color=(255, 255, 0)):
        """
        :param text: стартовый текст для отрисовки
        :param pos: кортеж с координатами верхнего левого угла текста
        :param font_size: размер шрифта
        :param color: цвет для отрисовки
        """
        self.color = color
        self.font_size = font_size
        self.pos = pos
        self.__text = text
        self.__freeze = False

        pygame.font.init()
        self.set_text(self.__text)
        self.rect = pygame.Rect(pos[0] - self.image.get_width() // 2,
                                pos[1] - self.image.get_height() // 2,
                                *self.image.get_size())

    def freeze(self):
        """
        Замораживает текст, запрещая его изменять до момента разморозки
        """
        self.__freeze = True

    def unfreeze(self):
        """
        Снимает заморозку текста, позволяя его изменять
        :return:
        """
        self.__freeze = False

    def set_text(self, text: str):
        """
        Назначает текст для отрисовки
        :param text: текст, который будет отображён на экране
        """
        if self.__freeze:
            return
        self.__text = text

        self.font = pygame.font.SysFont('Roboto', self.font_size)
        self.image = self.font.render(self.__text, False, self.color)
