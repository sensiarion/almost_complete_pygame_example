import os
from typing import Tuple, List, Union, Sequence, Iterable

import pygame
from pygame.sprite import Sprite

from pygame_utils.core import Game, SpriteObject, SpriteGroup, RenderableObject

SIZE = (90, 90)


class Fon(SpriteObject):
    def __init__(self, coords: Tuple[int, int], size: Tuple[int, int], *groups: pygame.sprite.AbstractGroup):
        super().__init__('fon.jpg', coords, *groups, image_dir='data', size=size)


class FixedSizeSprite(SpriteObject):
    def __init__(self, image_path: str, coords: Tuple[int, int],
                 size: Tuple[int, int] = SIZE, image_dir: str = 'data/',
                 *groups: pygame.sprite.AbstractGroup):
        super().__init__(image_path, coords, size=size, image_dir=image_dir, *groups)

        w, h = self.image.get_size()
        self.rect = self.image.get_rect().move(
            w * coords[0], h * coords[1]
        )


class Box(FixedSizeSprite):
    def __init__(self, coords: Tuple[int, int], *groups: pygame.sprite.AbstractGroup):
        super().__init__('box.png', coords, *groups, image_dir='data', size=SIZE)


class Grass(FixedSizeSprite):
    def __init__(self, coords: Tuple[int, int], *groups: pygame.sprite.AbstractGroup):
        super().__init__('grass.png', coords, *groups, image_dir='data', size=SIZE)


class Player(FixedSizeSprite):
    def __init__(self, coords: Tuple[int, int], *groups: pygame.sprite.AbstractGroup):
        super().__init__('mar.png', coords, *groups, image_dir='data', size=SIZE)


class Level(SpriteGroup):
    directory = 'data/'

    player: Player

    def __init__(self, level_filename: str, *sprites: Union[Sprite, Sequence[Sprite]]):
        super().__init__(*sprites)
        self.full_name = os.path.join(self.directory, level_filename)

        if not os.path.exists(self.full_name):
            raise ValueError(f"Level file  not found in {self.full_name}")

        self.player_group = SpriteGroup()
        self.physical = SpriteGroup()
        self.background = SpriteGroup()

        self.level_size = self.generate_level(self.load_level())

    def load_level(self) -> List[str]:
        level = []
        with open(self.full_name, 'r') as f:
            for row in f:
                level.append(row.strip())

        max_width = max(map(len, level))

        return [i.ljust(max_width, '.') for i in level]

    def generate_level(self, level: List[str]) -> Tuple[int, int]:
        for y in range(len(level)):
            for x in range(len(level[y])):
                if level[y][x] == '.':
                    self.background.add(Grass((x, y)))
                elif level[y][x] == '#':
                    self.physical.add(Box((x, y)))
                elif level[y][x] == '@':
                    self.background.add(Grass((x, y)))

                    self.player = Player((x, y))
                    self.player_group.add(self.player)
        return x, y

    def get_sprite_group(self) -> Iterable[SpriteGroup]:
        return self.background, self.physical, self.player_group


class Camera:
    def __init__(self, game: 'MyGame'):
        self.game = game
        self.dx = 0
        self.dy = 0

    def apply(self, obj: Sprite):
        obj.rect.x += self.dx
        obj.rect.y += self.dy

    def update(self, target: Sprite):
        self.dx = -(target.rect.x + target.rect.w // 2 - self.game.width // 2)
        self.dy = -(target.rect.y + target.rect.h // 2 - self.game.height // 2)


class MyGame(Game):
    dump: Tuple[List[RenderableObject], List[SpriteGroup]]

    player: Player

    def setup(self):
        self.level = Level('level1.txt')
        self.load_level(self.level)
        self.player = self.level.player

        self.dump = self.clear()
        self.is_started = False

        self.camera = Camera(self)

        fon = Fon((0, 0), (self.width, self.height))
        fon_group = SpriteGroup()
        fon_group.add(fon)
        self.add_object(fon_group)

        self.add_handler(pygame.KEYDOWN, self.start_game)
        self.add_handler(pygame.KEYDOWN, self.player_move)

    def start_game(self, event):
        if event.key == pygame.K_RETURN:
            t = self.dump
            self.dump = self.clear()
            if t:
                self.set(*t)
            self.is_started = not self.is_started

    def draw(self, time_delta: int):
        pass

    def load_level(self, level: Level):
        for group in level.get_sprite_group():
            self.add_object(group)

    def can_move(self) -> bool:
        if pygame.sprite.spritecollideany(self.player, self.level.physical):
            return False
        return True

    def player_move(self, event):
        if not self.is_started:
            return

        key = event.key

        if key == pygame.K_a:
            self.player.rect.x -= SIZE[0]
            if not self.can_move():
                self.player.rect.x += SIZE[0]
        if key == pygame.K_d:
            self.player.rect.x += SIZE[0]
            if not self.can_move():
                self.player.rect.x -= SIZE[0]
        if key == pygame.K_w:
            self.player.rect.y -= SIZE[1]
            if not self.can_move():
                self.player.rect.y += SIZE[1]
        if key == pygame.K_s:
            self.player.rect.y += SIZE[1]
            if not self.can_move():
                self.player.rect.y -= SIZE[1]

    def update(self):
        if not self.is_started:
            return

        self.camera.update(self.player)
        for group in self.get_sprite_groups():
            for obj in group.sprites():
                self.camera.apply(obj)


if __name__ == '__main__':
    game = MyGame(800, 600, fps=30, name='Mario')
    game.run()
